package butler

import (
	"net/http"
)

type Request interface {
	HttpRequest() *http.Request
}

type Handler interface {
	ServeHTTP(http.ResponseWriter, Request) bool
}

type HandlerFunc func(http.ResponseWriter, Request) bool

func (f HandlerFunc) ServeHTTP(w http.ResponseWriter, r Request) bool {
	return f(w, r)
}

func HttpHandlerFuncToButlerHandlerFunc(h http.HandlerFunc, proceed bool) HandlerFunc {
	return HandlerFunc(func(w http.ResponseWriter, r Request) bool {
		h.ServeHTTP(w, r.HttpRequest())
		return proceed
	})
}

func HttpHandlerToButlerHandler(h http.Handler, proceed bool) Handler {
	return HandlerFunc(func(w http.ResponseWriter, r Request) bool {
		h.ServeHTTP(w, r.HttpRequest())
		return proceed
	})
}

var NotFoundHandler HandlerFunc = HttpHandlerFuncToButlerHandlerFunc(http.NotFound, false)

// Chains multiple handlers together
func Chain(handlers ...interface{}) HandlerFunc {
	return func(w http.ResponseWriter, r Request) bool {
		for _, h := range handlers {
			if handler, ok := h.(Handler); ok {
				if !handler.ServeHTTP(w, r) {
					return false
				}
			}
			if hf, ok := h.(HandlerFunc); ok {
				if !hf(w, r) {
					return false
				}
			}
			return InternalServerError(w, "Chain must contain Handler and HandlerFunc objects")
		}
		return true
	}
}

type RequestConstructor func(*http.Request) Request

type Butler struct {
	rc    RequestConstructor
	chain []Handler
}

func New(rc RequestConstructor, chain []Handler) *Butler {
	return &Butler{rc, chain}
}

func (b *Butler) ServeHTTP(w http.ResponseWriter, hr *http.Request) {
	r := b.rc(hr)
	for _, h := range b.chain {
		if !h.ServeHTTP(w, r) {
			break
		}
	}
}
