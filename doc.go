/*
Package butler provides a framework for writing chained http handlers. butler
makes it easy to implement request filters and response post processors, in
a clean and consistent way.

The net/http package provides a fixed http.Request object. Unfortunately, this
is not an interface which makes it hard to store request level context. This
ability would be useful to store authentication information, for example.

The excellent gorilla toolkit (http://www.gorillatoolkit.org/) provides some
useful libraries. But to work around the above restriction, they are forced to
use a context object. This is locked when reading and writing to keep it
thread-safe. Although I do not have benchmarks to prove the performance impact,
it seems to defeat the purpose of having concurrent goroutines.

butler allows the user to specify a custom request object, which should
implement the butler.Request interface. Here is an example of a request object,
which is just a simple wrapper over http.Request

  type SimpleReq struct {
    *http.Request
  }

  func (r *SimpleReq) HttpRequest() *http.Request {
    return r.Request
  }

The HttpRequest() method is needed to interoperate with the http library.

As a more useful example, here is a request from one of the source files in the
examples directory

  type AuthReq struct {
    *http.Request
    user string
    vars map[string]string
  }

Here we have some extra information along with the request. The user field
contains information of the currently logged in user. The vars field is used to
store patterns matched from the url. Please look at the source of the example
for more details.

This request object must implement the butler.Request interface. Here is what
that might look like

  func (r *AuthReq) HttpRequest() *http.Request {
    return r.Request
  }

Given an arbitrary user defined request, butler needs a way to construct this
request from a regular http.Request. This constructor is passed to butler,
which instantiates it as needed. Usually, you would just set the http.Request
member of your request object. Continuing our example, here is the constructor
for AuthReq

  func NewAuthReq(r *http.Request) butler.Request {
    return &AuthReq{Request: r}
  }

Now here is an example handler

  func hello(w http.ResponseWriter, br butler.Request) bool {
    r, _ := br.(*AuthReq)
    fmt.Fprintf(w, "Hello %s and %s!", r.user, r.vars["name"])
    return true
  }

It is quite pleasant to work with. The only ugliness is in the first line. This
can also be removed, look at examples/hello
*/
package butler
