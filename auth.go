package butler

import (
	"encoding/base64"
	"github.com/gorilla/securecookie"
	"net/http"
	"strings"
)

type AuthRequest interface {
	Request
	SetUser(user interface{})
	GetUser() interface{}
}

type AuthenticatorFunc func(username, password string) (valid bool, user interface{})

type HTTPBasicAuthHandler struct {
	// Function returns (true, user) if valid, else (false, nil)
	authenticate AuthenticatorFunc
}

func NewHTTPBasicAuthHandler(f AuthenticatorFunc) *HTTPBasicAuthHandler {
	return &HTTPBasicAuthHandler{f}
}

func (hba *HTTPBasicAuthHandler) ServeHTTP(w http.ResponseWriter, br Request) bool {
	ar, ok := br.(AuthRequest)
	if !ok {
		http.Error(w, "Server error", 500)
		return false
	}
	if ar.GetUser() != nil {
		// Already authenticated. Skip
		return true
	}
	var b []byte
	var err error
	var pair []string
	var user interface{}
	r := ar.HttpRequest()
	s := strings.SplitN(r.Header.Get("Authorization"), " ", 2)
	if len(s) != 2 || s[0] != "Basic" {
		return true
	}
	b, err = base64.StdEncoding.DecodeString(s[1])
	if err != nil {
		return true
	}
	pair = strings.SplitN(string(b), ":", 2)
	if len(pair) != 2 {
		return true
	}
	_, user = hba.authenticate(pair[0], pair[1])
	if user != nil {
		ar.SetUser(user)
	}
	return true
}

type CookieAuthHandler struct {
	name               string
	sc                 *securecookie.SecureCookie
	backoffAuthHandler Handler
}

func NewCookieAuthHandler(name string, hashKey, blockKey []byte, backoffAuthHandler Handler) *CookieAuthHandler {
	return &CookieAuthHandler{name, securecookie.New(hashKey, blockKey), backoffAuthHandler}
}

func (cah *CookieAuthHandler) ServeHTTP(w http.ResponseWriter, br Request) bool {
	ar, ok := br.(AuthRequest)
	if !ok {
		http.Error(w, "Server error", 500)
		return false
	}
	if ar.GetUser() != nil {
		// Already authenticated. Skip
		return true
	}
	r := ar.HttpRequest()
	cookie, err := r.Cookie(cah.name)
	if err == nil {
		var user interface{}
		if err = cah.sc.Decode(cah.name, cookie.Value, user); err == nil {
			ar.SetUser(user)
			return true
		}
	} else if err != http.ErrNoCookie {
		http.Error(w, "Server error "+err.Error(), 500)
		return false
	}
	// Cookie auth did not work
	if cah.backoffAuthHandler != nil {
		proceed := cah.backoffAuthHandler.ServeHTTP(w, br)
		user := ar.GetUser()
		if user != nil {
			// Save this user to the cookie
			encoded, err := cah.sc.Encode(cah.name, user)
			if err == nil {
				cookie := &http.Cookie{
					Name:  cah.name,
					Value: encoded,
					Path:  "/",
				}
				http.SetCookie(w, cookie)
			} else {
				http.Error(w, "Server error "+err.Error(), 500)
				return false
			}
		}
		return proceed
	}
	return true
}

// An AuthRequiredFilter rejects requests that do not have authentication set.
// it checks that GetUser does not return nil
type AuthRequiredFilter struct {
	responseFunc func(http.ResponseWriter)
}

func NewAuthRequiredFilter(responseFunc func(http.ResponseWriter)) *AuthRequiredFilter {
	return &AuthRequiredFilter{responseFunc}
}

func (arf *AuthRequiredFilter) ServeHTTP(w http.ResponseWriter, br Request) bool {
	ar, ok := br.(AuthRequest)
	if !ok {
		return InternalServerError(w, "Request does not satisfy AuthRequest interface")
	}
	if ar.GetUser() != nil {
		// Already authenticated. Skip
		return true
	}
	if arf.responseFunc == nil {
		w.Header().Set("WWW-Authenticate", "Basic")
		w.WriteHeader(401)
		w.Write([]byte("401 Unauthorized\n"))
	} else {
		arf.responseFunc(w)
	}
	return false
}
