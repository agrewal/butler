package butler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
)

// An EncoderRequest contains the method GetData, which can return any object.
type EncoderRequest interface {
	Request
	GetData() interface{}
}

// A JsonEncoder is a butler component that needs an EncoderRequest. It just
// encodes the data in json and writes it to the output stream. It also sets
// the Content-Length and Content-Type headers automatically.
type JsonEncoder struct {
}

func NewJsonEncoder() *JsonEncoder {
	return &JsonEncoder{}
}

func (*JsonEncoder) ServeHTTP(w http.ResponseWriter, br Request) bool {
	er, ok := br.(EncoderRequest)
	if !ok {
		return InternalServerError(w, "Request does not conform to EncoderRequest interface")
	}
	data := er.GetData()
	output, err := json.Marshal(data)
	if err != nil {
		return InternalServerError(w, fmt.Sprintf("Error in json encoder : %s", err))
	}
	w.Header().Add("Content-Length", strconv.Itoa(len(output)))
	w.Header().Add("Content-Type", "application/json")
	_, err = w.Write(output)
	return true
}
