package main

import (
	"bitbucket.org/agrewal/butler"
	"fmt"
	"net/http"
)

type ExampleRequest struct {
	*http.Request
	vars map[string]string
}

func NewExampleRequest(r *http.Request) butler.Request {
	return &ExampleRequest{Request: r}
}

func (r *ExampleRequest) HttpRequest() *http.Request {
	return r.Request
}

func (r *ExampleRequest) SetMuxVars(vars map[string]string) {
	r.vars = vars
}

func ExampleRequestHandlerToButlerHandler(f func(http.ResponseWriter, *ExampleRequest) bool) func(http.ResponseWriter, butler.Request) bool {
	return func(w http.ResponseWriter, br butler.Request) bool {
		r, ok := br.(*ExampleRequest)
		if !ok {
			fmt.Fprintf(w, "error!!")
			return false
		}
		return f(w, r)
	}
}

func hello(w http.ResponseWriter, r *ExampleRequest) bool {
	fmt.Fprintf(w, "Hello %s!", r.vars["name"])
	return true
}

func bye(w http.ResponseWriter, r *ExampleRequest) bool {
	fmt.Fprintf(w, "Bye %s!", r.vars["name"])
	return true
}

func main() {
	p := butler.NewPatternServeMux()
	p.Get("/hello/:name", ExampleRequestHandlerToButlerHandler(hello))
	p.Get("/bye/:name", ExampleRequestHandlerToButlerHandler(bye))
	b := butler.New(NewExampleRequest, []butler.Handler{p})
	http.ListenAndServe(":8080", b)
}
