package main

import (
	"bitbucket.org/agrewal/butler"
	"fmt"
	"github.com/gorilla/securecookie"
	"net/http"
)

type AuthReq struct {
	*http.Request
	user string
	vars map[string]string
}

func NewAuthReq(r *http.Request) butler.Request {
	return &AuthReq{Request: r}
}

func (r *AuthReq) HttpRequest() *http.Request {
	return r.Request
}

func (r *AuthReq) SetMuxVars(vars map[string]string) {
	r.vars = vars
}

func (r *AuthReq) SetUser(user interface{}) {
	u, _ := user.(string)
	r.user = u
}

func (r *AuthReq) GetUser() interface{} {
	if r.user == "" {
		return nil
	}
	return r.user
}

func authenticate(username, password string) (bool, interface{}) {
	if username == "user" && password == "pass" {
		return true, username
	}
	return false, nil
}

func hello(w http.ResponseWriter, br butler.Request) bool {
	r, _ := br.(*AuthReq)
	fmt.Fprintf(w, "Hello %s and %s!", r.user, r.vars["name"])
	return true
}

func main() {
	a := butler.NewHTTPBasicAuthHandler(authenticate, true)
	hashKey, blockKey := securecookie.GenerateRandomKey(32), securecookie.GenerateRandomKey(16)
	c := butler.NewCookieAuthHandler("auth", hashKey, blockKey, a)
	p := butler.NewPatternServeMux()
	p.Get("/hello/:name", hello)
	b := butler.New(NewAuthReq, []butler.Handler{c, p})
	http.ListenAndServe(":8080", b)
}
