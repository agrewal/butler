package butler

import (
	"fmt"
	"net/http"
	"strconv"
)

// Simple wrapper over http.Error, that writes an error to the resposne. It
// sets the Content-Length flag, rather than using chunked transfer
func Error(w http.ResponseWriter, err interface{}, code int) bool {
	s := fmt.Sprintf("%v", err)
	w.Header().Add("Content-Length", strconv.Itoa(len(s)+1)) // Adding 1 for "\n"
	http.Error(w, s, code)
	return false
}

// Simple wrapper for an internal server error
func InternalServerError(w http.ResponseWriter, err interface{}) bool {
	return Error(w, err, http.StatusInternalServerError)
}

// Simple wrapper for an unauthorized error
func UnauthorizedError(w http.ResponseWriter, err interface{}) bool {
	return Error(w, err, http.StatusUnauthorized)
}

// Simple wrapper for a bad request error
func BadRequestError(w http.ResponseWriter, err interface{}) bool {
	return Error(w, err, http.StatusBadRequest)
}

// Simple wrapper for a conflict error
func ConflictError(w http.ResponseWriter, err interface{}) bool {
	return Error(w, err, http.StatusConflict)
}

// Simple wrapper for a gone error
func GoneError(w http.ResponseWriter, err interface{}) bool {
	return Error(w, err, http.StatusGone)
}

// Simple wrapper for a not found error
func NotFoundError(w http.ResponseWriter, err interface{}) bool {
	return Error(w, err, http.StatusNotFound)
}

// Tries to convert a given string to an int
func GetInt(s string) (int, error) {
	return strconv.Atoi(s)
}

// If the string is empty, then it returns default. Otherwise, it tries to
// convert the string to int
func GetIntOrDefault(s string, def int) (int, error) {
	if len(s) == 0 {
		return def, nil
	}
	return strconv.Atoi(s)
}

// Tries to convert a given string into a int64
func GetInt64(s string) (int64, error) {
	return strconv.ParseInt(s, 10, 64)
}

// If the string is empty, then it returns default. Otherwise, it tries to
// convert the string to int64
func GetInt64OrDefault(s string, def int64) (int64, error) {
	if len(s) == 0 {
		return def, nil
	}
	return strconv.ParseInt(s, 10, 64)
}
